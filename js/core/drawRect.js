(function() {
  'use strict';

  function DrawReact(canvas, width, height) {
    this.pointers = [];
    this.canvas = canvas;
    this.width = width;
    this.height = height;
  }

  DrawReact.prototype.createPointer = function(x, y, offsetX = 5, offsetY = 5) {
    this.pointer = { x, y, offsetX, offsetY };
  };

  DrawReact.prototype.incrementPointer = function() {
    let incrementX, incrementY;
    this.pointers.push(Object.assign({}, this.pointer));

    if (this.width + this.pointer.x < this.canvas.width - this.width) {
      incrementX = this.width + this.pointer.offsetX;
      incrementY = 0;
    } else if (
      this.height + this.pointer.y <
      this.canvas.height - this.height
    ) {
      incrementX = -this.pointer.x;
      incrementY = this.height + this.pointer.offsetY;
    } else {
      incrementX = -this.pointer.x;
      incrementY = -this.pointer.y;
    }
    this.pointer.x += incrementX;
    this.pointer.y += incrementY;
  };

  DrawReact.prototype.decrementPointer = function() {
    const lastPointer = this.pointers.pop();
    this.pointer.x = lastPointer.x;
    this.pointer.y = lastPointer.y;
  };

  DrawReact.prototype.resizePointer = function() {
    this.width += 5;
    this.height += 5;
  };

  DrawReact.prototype.createReact = function(color = '#000') {
    const ctx = this.canvas.getContext('2d');
    ctx.fillStyle = color;
    ctx.fillRect(this.pointer.x, this.pointer.y, this.width, this.height);
  };

  DrawReact.prototype.clearReact = function() {
    const ctx = this.canvas.getContext('2d');
    ctx.clearRect(this.pointer.x, this.pointer.y, this.width, this.height);
  };

  window.DrawReact = DrawReact;
})();
