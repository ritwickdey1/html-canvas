(function() {
  const mainCanvas = document.querySelector('#mainCanvas');
  const addBtn = document.querySelector('#addBtn');
  const resizeBtn = document.querySelector('#resizeBtn');
  const deleteBtn = document.querySelector('#deleteBtn');

  const offsetX = 5;
  const offsetY = 5;
  const width = Math.floor((mainCanvas.width - 4 * offsetX) / 4);
  const height = Math.floor((mainCanvas.height - 3 * offsetY) / 3);
  
  const drawReact = new DrawReact(mainCanvas, width, height);
  drawReact.createPointer(0, 0, offsetX, offsetY);

  addBtn.addEventListener('click', () => {
    drawReact.createReact(getRandomColor());
    drawReact.incrementPointer();
  });

  deleteBtn.addEventListener('click', () => {
    drawReact.decrementPointer();
    drawReact.clearReact();
  });
  
  resizeBtn.addEventListener('click', () => {
    drawReact.resizePointer();
  });
})();
